package org.openrdf.query.algebra.evaluation.function.geosparql;

import java.io.IOException;

import com.spatial4j.core.context.SpatialContext;
import com.spatial4j.core.context.jts.JtsSpatialContext;
import com.spatial4j.core.shape.Shape;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.WKTWriter;


public class SpatialSupportInitializer extends SpatialSupport {
	private final JtsSpatialContext context = JtsSpatialContext.GEO;
	
	@Override
	protected SpatialAlgebra createSpatialAlgebra() {
		return new JtsSpatialAlgebra(context);
	}

	@Override
	protected SpatialContext createSpatialContext() {
		return context;
	}

	@Override
	protected WktWriter createWktWriter() {
		return new JtsWktWriter(context);
	}



	static class JtsSpatialAlgebra implements SpatialAlgebra {
		private final JtsSpatialContext context;

		public JtsSpatialAlgebra(JtsSpatialContext context) {
			this.context = context;
		}

		public Shape convexHull(Shape s) {
			return context.makeShape(context.getGeometryFrom(s).convexHull());
		}

		public Shape boundary(Shape s) {
			return context.makeShape(context.getGeometryFrom(s).getBoundary());
		}

		public Shape envelope(Shape s) {
			return context.makeShape(context.getGeometryFrom(s).getEnvelope());
		}

		public Shape union(Shape s1, Shape s2) {
			return context.makeShape(context.getGeometryFrom(s1).union(context.getGeometryFrom(s2)));
		}

		public Shape intersection(Shape s1, Shape s2) {
			return context.makeShape(context.getGeometryFrom(s1).intersection(context.getGeometryFrom(s2)));
		}

		public Shape symDifference(Shape s1, Shape s2) {
			return context.makeShape(context.getGeometryFrom(s1).symDifference(context.getGeometryFrom(s2)));
		}

		public Shape difference(Shape s1, Shape s2) {
			return context.makeShape(context.getGeometryFrom(s1).difference(context.getGeometryFrom(s2)));
		}

		public boolean relate(Shape s1, Shape s2, String intersectionPattern) {
			return context.getGeometryFrom(s1).relate(context.getGeometryFrom(s2), intersectionPattern);
		}

		public boolean equals(Shape s1, Shape s2) {
			return context.getGeometryFrom(s1).equalsNorm(context.getGeometryFrom(s2));
		}

		public boolean sfDisjoint(Shape s1, Shape s2) {
			return relate(s1, s2, "FF*FF****");
		}

		public boolean sfIntersects(Shape s1, Shape s2) {
			return relate(s1, s2, "T********") || relate(s1, s2, "*T*******") || relate(s1, s2, "***T*****") || relate(s1, s2, "****T****");
		}

		public boolean sfTouches(Shape s1, Shape s2) {
			return relate(s1, s2, "FT*******") || relate(s1, s2, "F**T*****") || relate(s1, s2, "F***T****");
		}

		public boolean sfCrosses(Shape s1, Shape s2) {
			Geometry g1 = context.getGeometryFrom(s1);
			Geometry g2 = context.getGeometryFrom(s2);
			int d1 = g1.getDimension();
			int d2 = g2.getDimension();
			if((d1 == 0 && d2 == 1) || (d1 == 0 && d2 == 2) || (d1 == 1 && d2 == 2)) {
				return g1.relate(g2, "T*T***T**");
			}
			else if(d1 == 1 && d2 == 1) {
				return g1.relate(g2, "0*T***T**");
			}
			else {
				return false;
			}
		}

		public boolean sfWithin(Shape s1, Shape s2) {
			return relate(s1, s2, "T*F**F***");
		}

		public boolean sfContains(Shape s1, Shape s2) {
			return relate(s1, s2, "T*****FF*");
		}

		public boolean sfOverlaps(Shape s1, Shape s2) {
			Geometry g1 = context.getGeometryFrom(s1);
			Geometry g2 = context.getGeometryFrom(s2);
			int d1 = g1.getDimension();
			int d2 = g2.getDimension();
			if((d1 == 2 && d2 == 2) || (d1 == 0 && d2 == 0)) {
				return g1.relate(g2, "T*T***T**");
			}
			else if(d1 == 1 && d2 == 1) {
				return g1.relate(g2, "1*T***T**");
			}
			else {
				return false;
			}
		}

		public boolean ehDisjoint(Shape s1, Shape s2) {
			return relate(s1, s2, "FF*FF****");
		}

		public boolean ehMeet(Shape s1, Shape s2) {
			return relate(s1, s2, "FT*******") || relate(s1, s2, "F**T*****") || relate(s1, s2, "F***T****");
		}

		public boolean ehOverlap(Shape s1, Shape s2) {
			return relate(s1, s2, "T*T***T**");
		}

		public boolean ehCovers(Shape s1, Shape s2) {
			return relate(s1, s2, "T*TFT*FF*");
		}

		public boolean ehCoveredBy(Shape s1, Shape s2) {
			return relate(s1, s2, "TFF*TFT**");
		}

		public boolean ehInside(Shape s1, Shape s2) {
			return relate(s1, s2, "TFF*FFT**");
		}

		public boolean ehContains(Shape s1, Shape s2) {
			return relate(s1, s2, "T*TFF*FF*");
		}

		public boolean rcc8dc(Shape s1, Shape s2) {
			return relate(s1, s2, "FFTFFTTTT");
		}

		public boolean rcc8ec(Shape s1, Shape s2) {
			return relate(s1, s2, "FFTFTTTTT");
		}

		public boolean rcc8po(Shape s1, Shape s2) {
			return relate(s1, s2, "TTTTTTTTT");
		}

		public boolean rcc8tppi(Shape s1, Shape s2) {
			return relate(s1, s2, "TTTFTTFFT");
		}

		public boolean rcc8tpp(Shape s1, Shape s2) {
			return relate(s1, s2, "TFFTTFTTT");
		}

		public boolean rcc8ntpp(Shape s1, Shape s2) {
			return relate(s1, s2, "TFFTFFTTT");
		}

		public boolean rcc8ntppi(Shape s1, Shape s2) {
			return relate(s1, s2, "TTTFFTFFT");
		}
		
	}



	static class JtsWktWriter implements WktWriter {
		private final JtsSpatialContext context;

		public JtsWktWriter(JtsSpatialContext context) {
			this.context = context;
		}

		public String toWkt(Shape s)
			throws IOException
		{
			return new WKTWriter().write(context.getGeometryFrom(s));
		}
	}
}
