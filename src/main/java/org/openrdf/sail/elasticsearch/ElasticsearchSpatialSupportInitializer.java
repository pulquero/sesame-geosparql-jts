package org.openrdf.sail.elasticsearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.common.geo.builders.PolygonBuilder;
import org.elasticsearch.common.geo.builders.ShapeBuilder;

import com.spatial4j.core.shape.Shape;
import com.spatial4j.core.shape.jts.JtsGeometry;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;


public class ElasticsearchSpatialSupportInitializer extends ElasticsearchSpatialSupport {

	@Override
	protected ShapeBuilder toShapeBuilder(Shape s) {
		if(s instanceof JtsGeometry) {
			Geometry g = ((JtsGeometry)s).getGeom();
			if(g instanceof Polygon) {
				Polygon pg = (Polygon) g;
				PolygonBuilder pb = ShapeBuilder.newPolygon();
				for(Coordinate coords : pg.getCoordinates()) {
					pb.point(coords);
				}
				return pb;
			}
		}
		throw new UnsupportedOperationException("TODO: "+s.getClass().getName());
	}

	@Override
	protected Map<String,Object> toGeoJSON(Shape s) {
		if(s instanceof JtsGeometry) {
			Geometry g = ((JtsGeometry)s).getGeom();
			if(g instanceof Polygon) {
				Polygon pg = (Polygon) g;
				Map<String, Object> xb = new HashMap<String,Object>();
				List<Object[]> coordList = new ArrayList<Object[]>();
				for(Coordinate coords : pg.getCoordinates()) {
					coordList.add(new Object[] {coords.x, coords.y});
				}
				xb.put("type", "polygon");
				xb.put("coordinates", new Object[] {coordList});
					return xb;
			}
		}
		throw new UnsupportedOperationException("TODO: "+s.getClass().getName());
	}
}
